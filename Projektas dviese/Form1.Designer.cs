﻿namespace Projektas_dviese
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pliusBtn = new System.Windows.Forms.Button();
            this.minusBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.kartBtn = new System.Windows.Forms.Button();
            this.dalybaBtn = new System.Windows.Forms.Button();
            this.pakeltaBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pliusBtn
            // 
            this.pliusBtn.Location = new System.Drawing.Point(17, 51);
            this.pliusBtn.Name = "pliusBtn";
            this.pliusBtn.Size = new System.Drawing.Size(73, 62);
            this.pliusBtn.TabIndex = 11;
            this.pliusBtn.Text = "+";
            this.pliusBtn.UseVisualStyleBackColor = true;
            this.pliusBtn.Click += new System.EventHandler(this.pliusBtn_Click);
            // 
            // minusBtn
            // 
            this.minusBtn.Location = new System.Drawing.Point(96, 51);
            this.minusBtn.Name = "minusBtn";
            this.minusBtn.Size = new System.Drawing.Size(73, 62);
            this.minusBtn.TabIndex = 12;
            this.minusBtn.Text = "-";
            this.minusBtn.UseVisualStyleBackColor = true;
            this.minusBtn.Click += new System.EventHandler(this.minusBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label3.Location = new System.Drawing.Point(213, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 25);
            this.label3.TabIndex = 18;
            this.label3.Text = "=";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(17, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(77, 20);
            this.textBox1.TabIndex = 19;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(130, 12);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(77, 20);
            this.textBox2.TabIndex = 20;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(243, 12);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(85, 20);
            this.textBox3.TabIndex = 21;
            // 
            // kartBtn
            // 
            this.kartBtn.Location = new System.Drawing.Point(175, 51);
            this.kartBtn.Name = "kartBtn";
            this.kartBtn.Size = new System.Drawing.Size(73, 62);
            this.kartBtn.TabIndex = 22;
            this.kartBtn.Text = "*";
            this.kartBtn.UseVisualStyleBackColor = true;
            this.kartBtn.Click += new System.EventHandler(this.kartBtn_Click);
            // 
            // dalybaBtn
            // 
            this.dalybaBtn.Location = new System.Drawing.Point(255, 51);
            this.dalybaBtn.Name = "dalybaBtn";
            this.dalybaBtn.Size = new System.Drawing.Size(73, 62);
            this.dalybaBtn.TabIndex = 23;
            this.dalybaBtn.Text = "/";
            this.dalybaBtn.UseVisualStyleBackColor = true;
            this.dalybaBtn.Click += new System.EventHandler(this.dalybaBtn_Click);
            // 
            // pakeltaBtn
            // 
            this.pakeltaBtn.Location = new System.Drawing.Point(17, 119);
            this.pakeltaBtn.Name = "pakeltaBtn";
            this.pakeltaBtn.Size = new System.Drawing.Size(73, 62);
            this.pakeltaBtn.TabIndex = 24;
            this.pakeltaBtn.Text = "^";
            this.pakeltaBtn.UseVisualStyleBackColor = true;
            this.pakeltaBtn.Click += new System.EventHandler(this.pakeltaBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(100, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 25);
            this.label1.TabIndex = 25;
            this.label1.Text = "=";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 197);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pakeltaBtn);
            this.Controls.Add(this.dalybaBtn);
            this.Controls.Add(this.kartBtn);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.minusBtn);
            this.Controls.Add(this.pliusBtn);
            this.Name = "Form1";
            this.Text = "x";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button pliusBtn;
        private System.Windows.Forms.Button minusBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button kartBtn;
        private System.Windows.Forms.Button dalybaBtn;
        private System.Windows.Forms.Button pakeltaBtn;
        private System.Windows.Forms.Label label1;
    }
}

