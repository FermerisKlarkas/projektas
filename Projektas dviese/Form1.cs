﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projektas_dviese
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        decimal x, y;
        decimal Pliusavimas(decimal x,decimal y)
        {
            return x + y;
        }

        decimal Minusavimas(decimal x, decimal y)
        {
            return x - y;
        }
        decimal Dalyba(decimal x, decimal y)
        {
            return x / y;
        }
        decimal Daugyba(decimal x, decimal y)
        {
            return x * y;
        }

        private void pliusBtn_Click(object sender, EventArgs e)
        {
            textBox3.Text = Pliusavimas(Int32.Parse(textBox1.Text), Int32.Parse(textBox2.Text)).ToString();
        }

        private void minusBtn_Click(object sender, EventArgs e)
        {
            textBox3.Text = Minusavimas(Int32.Parse(textBox1.Text), Int32.Parse(textBox2.Text)).ToString();
        }

        private void kartBtn_Click(object sender, EventArgs e)
        {
            textBox3.Text = Daugyba(Int32.Parse(textBox1.Text), Int32.Parse(textBox2.Text)).ToString();
        }

        private void dalybaBtn_Click(object sender, EventArgs e)
        {
            textBox3.Text = Dalyba(Int32.Parse(textBox1.Text), Int32.Parse(textBox2.Text)).ToString();
        }

        private void pakeltaBtn_Click(object sender, EventArgs e)
        {
            textBox3.Text = KelimasLaipsniu(Int32.Parse(textBox1.Text), Int32.Parse(textBox2.Text)).ToString();
        }

        long KelimasLaipsniu(int x, int y)
        {
            long ats=x;
            for(int i=0;i<y;i++)
            {
                ats=ats * x;
            }
            return ats;
        }
    }
}
